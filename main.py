import sys
import time
import os
import xml.etree.ElementTree as ET
from pynput import keyboard
from tkinter import Tk, Label
from PIL import Image, ImageTk
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def web_browser_fullscreen_shower(url):
    chrome_options = Options()
    chrome_options.add_argument("--kiosk")
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(chrome_options)
    driver.get(url)
    windows_handles = driver.window_handles
    return driver, windows_handles


def web_browser_hide(driver):
    driver.minimize_window()


def web_browser_show(driver):
    driver.fullscreen_window()


def web_driver_chrome(driver, win_handles):
    target = win_handles[0]
    driver.switch_to.window(target)
    return driver


def on_press(key):
    try:
        if key == keyboard.Key.ctrl_l:
            global isNextCycle
            isNextCycle = False
    except AttributeError:
        print("AttributeError")


def pictures_list_update(pictures_path):
    pictures_list = [pictures_path + i for i in os.listdir(pictures_path) if
                     i.endswith(".png") or i.endswith("jpg") or i.endswith("jpeg")]

    pic_counter = len(pictures_list)

    photos = list(range(pic_counter))

    for i in range(pic_counter):
        photos[i] = ImageTk.PhotoImage(
            Image.open(pictures_list[i]).resize((tk_root.winfo_width(), tk_root.winfo_height())))

    return pictures_list, pic_counter, photos


def cycle_image(photos_array, pc_len):
    global root
    label.config(image=photos_array[pc_len])
    time.sleep(float(root[1][0].text))
    label.update()


isNextCycle = True
tree = ET.parse("option.xml")
root = tree.getroot()
tk_root = Tk()
tk_root.overrideredirect(False)

web_link = root[0][0].text
pictures_path = root[0][1].text

web_browser, web_handle = web_browser_fullscreen_shower(web_link)
web_driver = web_driver_chrome(web_browser, web_handle)

tk_root.attributes("-fullscreen", True)

label = Label(tk_root)
label.pack()
tk_root.update()
isFirstCycle = False

with keyboard.Listener(on_press=on_press) as listener:
    while isNextCycle:
        pictures_list, pic_counter, photos = pictures_list_update(pictures_path)
        label.config(image=photos[0])
        tk_root.update()
        web_browser_hide(web_driver)

        photos_len = 0
        while photos_len < len(photos):
            cycle_image(photos, photos_len)
            photos_len += 1
            if photos_len == len(photos):
                label.quit()

        time.sleep(float(root[1][1].text))
        web_browser_show(web_driver)
        time.sleep(float(root[1][1].text))

tk_root.destroy()
web_driver.quit()
sys.exit()
listener.join()
